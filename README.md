# app-ipfs

**This was formally named app-clearshare. If this project is resurrected, it needs to be reworked to take the name change into account.**

App-ipfs (formerly App ClearSHARE) is a file sharing stack that incorporates IPFS file sharing, a marketplace where storage can be exchanged using Clear tokens, and an easy to use interface for configuring the system.

Known issues

1. Large files not supported to the IPFS cluster.
