<?php

/**
 * ClearSHARE project controller.
 *
 * @category   apps
 * @package    clearshare
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Docker project controller.
 *
 * @category   apps
 * @package    clearshare
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

class Project extends ClearOS_Controller
{
    protected $project_name = NULL;

    /**
     * ClearSHARE project constructor.
     *
     * @param string $project_name project name
     *
     * @return view
     */

    function __construct()
    {
        $this->project_name = 'clearshare';
    }

    /**
     * Project status.
     *
     * @return view
     */

    function status()
    {

        $this->load->library('docker/Project', 'clearshare');

        $status['status'] = $this->project->get_status();

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        echo json_encode($status);
    }

    /**
     * Project start.
     *
     * @return view
     */

    function start()
    {
        $this->load->library('docker/Project', 'clearshare');

        try {
            $this->project->set_running_state(TRUE, FALSE);
        } catch (Exception $e) {
            //
        }

        //sleep(5);

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        echo json_encode('ok');
    }

    /**
     * Project stop.
     *
     * @return view
     */

    function stop()
    {
        $this->load->library('docker/Project', 'clearshare');

        try {
            $this->project->set_running_state(FALSE);
        } catch (Exception $e) {
            //
        }

        // Wait for container stop
        sleep(10);

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        echo json_encode('ok');
    }
}
