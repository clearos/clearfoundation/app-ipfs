<?php

/**
 * ClearSHARE controller.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage controller
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearSHARE controller.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage controller
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 */

class Clearshare extends ClearOS_Controller
{
    /**
     * ClearSHARE default controller.
     *
     * @return view
     */

    function index()
    {
        // Load libraries
        //------------------

        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');

        try {
            $data['storage'] = $this->clearshare->get_local_storage();
            $data['pins_files'] = $this->clearshare->get_list_pins_files();
            $data['basedirs'] = $this->clearshare->get_basedirs();

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        } 
        
        // Load views
        //-----------
        $this->page->view_form('clearshare', $data, lang('clearshare_app_name'));
    }

    /**
     * Add the file included in the POST to the IPFS cluster
     *
     * @return view
     */

    function add_file()
    {
        // Load libraries
        //------------------

        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');

        try {

            if ($_POST) {
                
                $name = $_FILES['key_file']['name'];
                $tmp_name = $_FILES['key_file']['tmp_name'];
                $size = $_FILES['key_file']['size'];
                
                $response = $this->clearshare->add_file_to_ipfs_cluster($name, $tmp_name, $size);

                redirect('/clearshare');
            }

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------
        $this->page->view_form('add_file', NULL, lang('clearshare_app_name'));
    }

    /**
     * Pin the specified IPFS address to the local node
     *
     * @return view
     */

    function add_pins()
    {
        // Load libraries
        //------------------

        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');

        try {

            if ($_POST) {

                $cid = $this->input->post('cid');
                
                $response = $this->clearshare->put_pins_file($cid);

                redirect('/clearshare');
            }

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------
        $this->page->view_form('add_pins', NULL, lang('clearshare_app_name'));
    }

    /**
    * Confirm Delete pins View
    *
    * @param string $cid cid for file
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function confirm_delete_pins($cid)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');
      
        // confirm uri
        //---------------
        $confirm_uri = '/app/clearshare/delete_pins/' . $cid;
        $cancel_uri = '/app/clearshare/';

        $this->page->view_confirm(lang('clearshare_confirm_delete_cid') . "<b> $cid </b>?", $confirm_uri, $cancel_uri);
    }

    /**
    *  Delete pins
    *
    * @param string $cid cid for file
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function delete_pins($cid)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');
      
        try {
            $this->clearshare->unpins_file($cid);
            redirect('/clearshare');

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        } 
    }

    /**
     * Add an additional base directory to the list of configured ones
     *
     * @return view
     */

    function add_directory()
    {
        // Load libraries
        //------------------

        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');

        try {

            if ($_POST) {

                $base_dir_key = $this->input->post('base_dir_key');
                $base_dir_path = $this->input->post('base_dir_path');
                
                $response = $this->clearshare->add_base_directory($base_dir_key, $base_dir_path);

                redirect('/clearshare');
            }

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------
        $this->page->view_form('add_base_directory', NULL, lang('clearshare_app_name'));
    }

    /**
    * Delete base directory View
    *
    * @param string $key key for directory
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function confirm_delete_base_directory($key)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');
      
        // confirm uri
        //---------------
        $confirm_uri = '/app/clearshare/delete_base_directory/' . $key;
        $cancel_uri = '/app/clearshare/';

        $this->page->view_confirm(lang('clearshare_confirm_delete_key') . "<b> $key </b>?", $confirm_uri, $cancel_uri);
    }

    /**
    * Remove the base directory configured under `key` from the *live* server instance
    *
    * @param string $key key for directory
    *
    * @return redirect
    * @throws Engine_Exception
    */

    function delete_base_directory($key)
    {
        clearos_profile(__METHOD__, __LINE__);

        // Load libraries
        //---------------
        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');
      
        try {
            $this->clearshare->delete_directory($key);
            redirect('/clearshare');

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        } 
    }

    /**
     * Get the contents of directory in the cluster storage
     *
     * @param string $key key for directory
     *
     * @return view
     */

    function list_contents($key)
    {
        // Load libraries
        //------------------

        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');

        try {
            $data['storage'] = $this->clearshare->get_list_contents($key);

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------
        $this->page->view_form('list_contents', $data, lang('clearshare_app_name'));
    }

    /**
     * Add the local file included in the PUT request path to the IPFS cluster 
     *
     * @return view
     */

    function put_file()
    {
        // Load libraries
        //------------------

        $this->lang->load('clearshare');
        $this->load->library('clearshare/Clearshare');

        try {

            if ($_POST) {

                $name = $_POST['name_file'];
                $request_file_path = $_POST['request_file_path'];
                
                $response = $this->clearshare->put_file_to_ipfs_cluster($name, $request_file_path);
                redirect('/clearshare');
            }

        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------
        $this->page->view_form('put_file', NULL, lang('clearshare_app_name'));
    }
}
