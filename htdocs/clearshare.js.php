<?php

/**
 * ClearSHARE javascript helper.
 *
 * @category   apps
 * @package    clearshare
 * @subpackage javascript
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/storage/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('base');
clearos_load_language('clearshare');

///////////////////////////////////////////////////////////////////////////////
// J A V A S C R I P T
///////////////////////////////////////////////////////////////////////////////

header('Content-Type:application/x-javascript');
?>
var last_state = "";

///////////////////////////////////////////////////////////////////////////////
// F U N C T I O N S
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////
//// Start / Stop Daemon
////////////////////////////////////////////

$(document).ready(function() {
    var os_project_name = 'clearshare';
    var os_app_name = 'clearshare';

    clearosProject(os_project_name, os_app_name);

    $('#sidebar_daemon_status').show();
    $('#sidebar_daemon_action').show();

    // Click Events
    //-------------

    $('#os_daemon_action').click(function(e) {
        e.preventDefault();
        var service_status = $("#os_daemon_action").html();
        $("#os_daemon_action").html(clearos_loading());

        var options = new Object();
        options.classes = 'theme-daemon-start-stop-status';

        if ($('#os_project_status_lock').val() == 'on') {
            // Prevent double click
        } else if (service_status == lang_stop) {
            $('#os_project_status_lock').val('on');
            $('#os_daemon_status').html(clearos_loading(options) + lang_stopping + '...');
            clearosStopProject(os_project_name);
        } else {
            $('#os_project_status_lock').val('on');
            $('#os_daemon_status').html(clearos_loading(options) + lang_starting + '...');
            clearosStartProject(os_project_name);
        }
    });
});

///////////////////////////////////////////////////////////////////////////
// D A E M O N
///////////////////////////////////////////////////////////////////////////

function clearosProject(daemon, app_name) {

    // Translations
    //-------------

    lang_busy = '<?php echo lang("base_busy"); ?>';
    lang_restart = '<?php echo lang("base_restart"); ?>';
    lang_restarting = '<?php echo lang("base_restarting"); ?>';
    lang_running = '<?php echo lang("base_running"); ?>';
    lang_start = '<?php echo lang("base_start"); ?>';
    lang_starting = '<?php echo lang("base_starting"); ?>';
    lang_stop = '<?php echo lang("base_stop"); ?>';
    lang_stopping = '<?php echo lang("base_stopping"); ?>';
    lang_stopped = '<?php echo lang("base_stopped"); ?>';
    lang_dead = '<?php echo lang("base_dead"); ?>';
    lang_no_entries = '<?php echo lang("base_no_entries"); ?>';
    basename = '/app/' + app_name + '/project';

    $('#os_daemon_status').html('');

    clearosGetProjectStatus();
}

// Functions
//----------

function clearosStartProject(daemon) {
    $.ajax({
        url: basename + '/start', 
        method: 'GET',
        dataType: 'json',
        success : function(payload) {
            $('#os_project_status_lock').val('off');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#os_project_status_lock').val('off');
            console.log(errorThrown);
        }
    });
}

function clearosStopProject(project) {
    $.ajax({
        url: basename + '/stop', 
        method: 'GET',
        dataType: 'json',
        success : function(payload) {
            $('#os_project_status_lock').val('off');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#os_project_status_lock').val('off');
            console.log(errorThrown);
        }
    });
}

function clearosGetProjectStatus() {

    $.ajax({
        url: basename + '/status', 
        method: 'GET',
        dataType: 'json',
        success : function(payload) {
            var os_project_status_lock = $('#os_project_status_lock').val();
            //if (os_project_status_lock == 'off')
                clearosShowProjectStatus(payload);
               
            if (last_state && (last_state != payload.status)) {
           
                window.location.reload(true);
            }
            last_state = payload.status;
            window.setTimeout(clearosGetProjectStatus, 3000);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            window.setTimeout(clearosGetProjectStatus, 1000);
        }
    });
}

function clearosShowProjectStatus(payload) {
    if (payload.status == 'running') {
        $("#os_daemon_status").html(lang_running);
        $("#os_daemon_action").html(lang_stop);
        $("#clearos_container_running").show();
    } else if (payload.status == 'stopped') {
        $("#os_daemon_status").html("<span class='theme-stopped-daemon'>" + lang_stopped + "</span>");
        $("#os_daemon_action").html(lang_start);
        $("#clearos_container_running").hide();
    } else if (payload.status == 'dead') {
        $("#os_daemon_status").html("<span class='theme-dead-daemon'>" + lang_dead + "</span>");
        $("#os_daemon_action").html(lang_start);
        $("#clearos_container_running").hide();
    } else if (payload.status == 'starting') {
        $('#os_daemon_status').html(lang_starting + '<span class="theme-loading"></span>');
        $("#os_daemon_action").html('<span class="theme-loading-small" style="padding-right: 5px; height: 15px; margin-bottom: -5px;"></span>');
        $("#clearos_container_running").hide();
        $(".theme-loading-small").css('background-position', '5 0');
    } else if (payload.status == 'stopping') {
        $('#os_daemon_status').html(lang_stopping + '<span class="theme-loading"></span>');
        $("#os_daemon_action").html('<span class="theme-loading-small" style="padding-right: 5px; height: 15px; margin-bottom: -5px;"></span>');
        $("#clearos_container_running").hide();
        $(".theme-loading-small").css('background-position', '5 0');
    } else if (payload.status == 'restarting') {
        $('#os_daemon_status').html(lang_restarting + '<span class="theme-loading"></span>');
        $("#os_daemon_action").html('<span class="theme-loading-small" style="padding-right: 5px; height: 15px; margin-bottom: -5px;"></span>');
        $("#clearos_container_running").hide();
        $(".theme-loading-small").css('background-position', '5 0');
    } else if (payload.status == 'busy') {
        $('#os_daemon_status').html("<span class='theme-stopped-daemon'>" + clearos_loading({text: lang_busy}) + "</span>");
        $("#os_daemon_action").html(lang_stop);
        $("#clearos_container_running").hide();
        $(".theme-loading-small").css('background-position', '5 0');
    } else if (payload.status == 'no_entries') {
        $("#os_daemon_status").html(lang_no_entries);
        $("#os_daemon_action_row").css('display', 'none');
        $("#clearos_container_running").hide();
    }
}
