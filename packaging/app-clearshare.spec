
Name: app-clearshare
Epoch: 1
Version: 1.0.7
Release: 1%{dist}
Summary: ClearSHARE
License: GPLv3
Group: Applications/Apps
Packager: ClearFoundation
Vendor: ClearFoundation
Source: %{name}-%{version}.tar.gz
#Source2: clearshare.service
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: app-docker >= 2.6.1

%description
ClearSHARE - a file sharing stack that incorporates IPFS file sharing, a marketplace where storage can be exchanged using Clear tokens, and an easy to use interface for configuring the system.

%package core
Summary: ClearSHARE - API
License: GPLv3
Group: Applications/API
Requires: app-base-core
Requires: curl
Requires: app-docker-core

%description core
ClearSHARE - a file sharing stack that incorporates IPFS file sharing, a marketplace where storage can be exchanged using Clear tokens, and an easy to use interface for configuring the system.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/clearshare
cp -r * %{buildroot}/usr/clearos/apps/clearshare/
rm -f %{buildroot}/usr/clearos/apps/clearshare/README.md

install -d -m 0755 %{buildroot}/var/clearos/clearshare
install -d -m 0755 %{buildroot}/var/clearos/docker/project
install -d -m 0755 %{buildroot}/var/lib/clearshare
install -d -m 0755 %{buildroot}/usr/lib/systemd/system
install -D -m 0644 packaging/clearshare.php %{buildroot}/var/clearos/docker/project/clearshare.php
install -D -m 0644 packaging/docker-compose.yml %{buildroot}/var/lib/clearshare/docker-compose.yml

# Install systemd service files
install -p -m 644 packaging/clearshare.service %{buildroot}/usr/lib/systemd/system/clearshare.service

%post
logger -p local6.notice -t installer 'app-clearshare - installing'

%post core
logger -p local6.notice -t installer 'app-clearshare-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/clearshare/deploy/install ] && /usr/clearos/apps/clearshare/deploy/install
fi

[ -x /usr/clearos/apps/clearshare/deploy/upgrade ] && /usr/clearos/apps/clearshare/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-clearshare - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-clearshare-api - uninstalling'
    [ -x /usr/clearos/apps/clearshare/deploy/uninstall ] && /usr/clearos/apps/clearshare/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/clearshare/controllers
/usr/clearos/apps/clearshare/htdocs
/usr/clearos/apps/clearshare/views

%files core
%defattr(-,root,root)
%doc README.md
%exclude /usr/clearos/apps/clearshare/packaging
%exclude /usr/clearos/apps/clearshare/unify.json
%dir /usr/clearos/apps/clearshare
%dir /var/clearos/clearshare
%dir /var/clearos/docker/project
%dir /var/lib/clearshare
/usr/clearos/apps/clearshare/deploy
/usr/clearos/apps/clearshare/language
/var/clearos/docker/project/clearshare.php
/var/lib/clearshare/docker-compose.yml
/usr/lib/systemd/system/clearshare.service
