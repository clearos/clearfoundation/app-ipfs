<?php

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// C O N F I G L E T
///////////////////////////////////////////////////////////////////////////////

$configlet = array(
    'title' => "ClearSHARE",
    'app_name' => 'clearshare',
    'base_project' => 'clearshare',
    'container_count' => 1,
    'docker_compose_file' => '/var/lib/clearshare/docker-compose.yml',
    'images' => [
        'clearfoundation/clearshare:latest' => 'clearshare',
    ]
);
