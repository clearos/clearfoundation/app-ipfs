<?php

/**
 * ClearSHARE add pins files form.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage View
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('clearshare');

//echo infobox_highlight(lang('clearshare_app_name'), lang('clearshare_app_description'));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FORM
///////////////////////////////////////////////////////////////////////////////


echo form_open('clearshare/add_pins');
echo form_header(lang('clearshare_add_pins'));
echo field_input('cid', '', lang('clearshare_file_cid'), FALSE);
echo field_button_set(
    array(form_submit_custom('submit', lang('clearshare_add_pins_button')), anchor_cancel('/app/clearshare'))
);

echo form_footer();
echo form_close();
