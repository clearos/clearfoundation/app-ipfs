<?php

/**
 * ClearSHARE Directory summary.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage View
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('clearshare');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('clearshare_files_name'), lang('clearshare_isdir'));

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$anchors = array(anchor_custom('/app/clearshare', lang('clearshare_back_summary')));


///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

foreach ($storage['items'] as $key => $value) {

    if ($value['isdir']) {
        $isdir = 'Yes';

    } else {
        $isdir = 'No';

    }

    $item['title'] = 'title';
    $item['action'] = '';
    $item['anchors'] = button_set(array());
    $item['details'] = array($value['url'], $isdir);

    $items[] = $item;
}

 sort($items);
 

///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'clearshare_directory_content',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    lang('clearshare_directory_content'),
    $anchors,
    $headers,
    $items,
    $options
);


