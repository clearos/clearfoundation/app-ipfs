<?php

/**
 * ClearSHARE storage summary.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage View
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('clearshare');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('clearshare_files_name'), lang('clearshare_isdir'));

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$anchors = array();


///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

foreach ($storage['items'] as $key => $value) {

    if ($value['isdir']) {
        $isdir = 'Yes';

    } else {
        $isdir = 'No';

    }

    $item['title'] = 'title';
    $item['action'] = '';
    $item['anchors'] = button_set(array());
    $item['details'] = array($value['url'], $isdir);

    $items[] = $item;
}

 sort($items);
 

///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'clearshare_files',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    lang('clearshare_storage'),
    $anchors,
    $headers,
    $items,
    $options
);


///////////////////////////////////////////////////////////////////////////////
// Headers IPFS cluster storage
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('clearshare_files_name'), lang('clearshare_file_cid'), lang('clearshare_file_size'));

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$anchors = array(anchor_custom('/app/clearshare/add_file', lang('clearshare_add_file')));


///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////


foreach ($pins_files['pins'] as $key => $value) {

    $cid = explode(":", $value['cid']);
    $cid = str_replace('}', ' ', $cid[1]);
    $cid = str_replace("'", ' ', $cid);

    $item1['title'] = 'title';
    $item1['action'] = '';
    $item1['anchors'] = button_set(array());
    $item1['details'] = array($value['name'], trim($cid),$value['shard_size']);

    $items1[] = $item1;
}

 sort($items1);
 

///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'clearshare_cluster_storages',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    lang('clearshare_ipfs_cluster_storage'),
    $anchors,
    $headers,
    $items1,
    $options
);


///////////////////////////////////////////////////////////////////////////////
// Headers  Pins file
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('clearshare_files_name'), lang('clearshare_file_cid'));

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$anchors = array(anchor_custom('/app/clearshare/add_pins', lang('clearshare_file_add_pins')));


///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

foreach ($pins_files['pins'] as $key => $value) {

    $cid = explode(":", $value['cid']);
    $cid = str_replace('}', ' ', $cid[1]);
    $cid = str_replace("'", ' ', $cid);

    $item2['title'] = 'title';
    $item2['action'] = '';
    $item2['anchors'] = button_set(array(anchor_custom('/app/clearshare/confirm_delete_pins/'.trim($cid), lang('clearshare_file_delete_pins'))));
    $item2['details'] = array($value['name'], trim($cid));

    $items2[] = $item2;
}

 sort($items2);
 

///////////////////////////////////////////////////////////////////////////////
// List pins file summary.
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'clearshare_pinss',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    lang('clearshare_pins_files'),
    $anchors,
    $headers,
    $items2,
    $options
);

///////////////////////////////////////////////////////////////////////////////
// Headers  Basedirs
///////////////////////////////////////////////////////////////////////////////

$headers = array(lang('clearshare_directory_key'), lang('clearshare_directory_path'));

///////////////////////////////////////////////////////////////////////////////
// Anchors 
///////////////////////////////////////////////////////////////////////////////

$anchors = array(anchor_custom('/app/clearshare/add_directory', lang('clearshare_add_directory')));


///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

foreach ($basedirs as $keys => $values) {

    foreach ($values as $key => $value) {

        $base_key = $value['key'];
        $item3['title'] = 'title';
        $item3['action'] = '';
        $item3['anchors'] = button_set(array(anchor_custom('/app/clearshare/list_contents/'.$base_key, lang('clearshare_directory_view_button')), anchor_custom('/app/clearshare/confirm_delete_base_directory/'.$base_key, lang('clearshare_directory_delete_button'))));
        $item3['details'] = array($value['key'], $value['path']);

        $items3[] = $item3;
    }
}

 sort($items3);
 

///////////////////////////////////////////////////////////////////////////////
// List pins file summary.
///////////////////////////////////////////////////////////////////////////////

$options = array(
    'id' => 'clearshare_base_dir',
    //'responsive' => array(1 => 'none')
);
echo summary_table(
    lang('clearshare_list_directories'),
    $anchors,
    $headers,
    $items3,
    $options
);
