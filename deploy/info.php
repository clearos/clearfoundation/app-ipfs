<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'clearshare';
$app['version'] = '1.0.7';
$app['release'] = '1';
$app['vendor'] = 'ClearFoundation'; // e.g. Acme Co
$app['packager'] = 'ClearFoundation'; // e.g. Gordie Howe
$app['license'] = 'GPLv3'; // e.g. 'GPLv3';
$app['license_core'] = 'GPLv3'; // e.g. 'LGPLv3';
$app['description'] = lang('clearshare_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('clearshare_app_name');
$app['category'] = lang('base_category_system');
$app['subcategory'] = lang('base_subcategory_storage');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['requires'] = array(
    'app-docker >= 2.6.1',
);


$app['core_requires'] = array(
    'curl',
    'app-docker-core',
);

$app['core_directory_manifest'] = array(
    '/var/clearos/clearshare' => array(),
    '/var/lib/clearshare' => array(),
    '/var/clearos/docker/project' => array(),
);

$app['core_file_manifest'] = array(
    'clearshare.php'=> array('target' => '/var/clearos/docker/project/clearshare.php'),
    'docker-compose.yml'=> array('target' => '/var/lib/clearshare/docker-compose.yml'),
);
