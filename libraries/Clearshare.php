<?php

/**
 * ClearSHARE class.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearshare/
 */

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\clearshare;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('clearshare');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;
use \clearos\apps\base\Folder as Folder;
use \clearos\apps\base\Shell as Shell;

clearos_load_library('base/Engine');
clearos_load_library('base/File');
clearos_load_library('base/Folder');
clearos_load_library('base/Shell');

// Exceptions
//-----------

use \Exception as Exception;
use \clearos\apps\base\Engine_Exception as Engine_Exception;
use \clearos\apps\base\Validation_Exception as Validation_Exception;

clearos_load_library('base/Engine_Exception');
clearos_load_library('base/Validation_Exception');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearSHARE class.
 *
 * @category   Apps
 * @package    ClearSHARE
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2019 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 */

class Clearshare extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const CURL_COMMAND = '/usr/bin/curl';
    const PATH_TMP = '/tmp';
    const API_BASE_URL = 'http://clearshare.local:8095/ipfs/data/';

    ///////////////////////////////////////////////////////////////////////////////
    // V A R I A B L E S
    ///////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////


    /**
     * ClearSHARE constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);
    }

    /**
     * Get the contents of `req_path` in the local cluster storage.
     *
     * @return array local cluster storage.
     */

    public function get_local_storage()
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------
        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
            
        $shell = new Shell();
        $retval = $shell->execute(self::CURL_COMMAND, ' -X POST "'.self::API_BASE_URL.'browse/home" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"page\": 0, \"pages\": 0, \"per_page\": 0, \"total\": 0}"', TRUE, $options);

        $lines = $shell->get_output();

        $error = (preg_match('/An unhandled exception occurred/', $lines[3])) ? 'An unhandled exception occurred!' : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);

        }

    }

    /**
     * Add the file included in the POST to the IPFS cluster
     *
     * @param string $name     file name
     * @param string $tmp_name file tmp name
     * @param init   $size     file size
     *
     * @return array local cluster storage.
     */

    public function add_file_to_ipfs_cluster($name, $tmp_name, $size)
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        if (!empty($tmp_name)) {
            $file = new File($tmp_name);
            $file->move_to(self::PATH_TMP . '/' . $name);
            $file->chmod('0775');
        }

        //-------------------------------
        // Upload file in tmp folder for get path

        $shell = new Shell();
        $file_path = self::PATH_TMP . '/' . $name; 

        //-------------------------------
        //POST to the IPFS cluster

        $retval = $shell->execute(self::CURL_COMMAND, ' -F "data=@'.$file_path.'" -X POST "'.self::API_BASE_URL.'files/'.$name.'?name='.$name.'&shard_size='.$size.'" -H "Content-Type: multipart/form-data"', TRUE, $options);
        $lines = $shell->get_output();

        $error = (preg_match('/An unhandled exception occurred/', $lines[3])) ? 'An unhandled exception occurred!' : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {

            // Pin the specified IPFS address to the local node
            //.................................................
            $value = json_decode($lines[3], TRUE);

            $cid = explode(":", $value[0]['cid']);
            $rep_brackets = str_replace('}', ' ', $cid[1]);
            $cid_comma = str_replace("'", ' ', $rep_brackets);
            $comma = '"';
            $cid = str_replace($comma, ' ', $cid_comma);

            $this->put_pins_file($cid);

            return $this->_filter_response($lines, 3);
        }
    }

    /**
     * List all the pins that are allocated at each node in the IPFS cluster
     *
     * @return array List all the pins that are allocated at each node.
     */

    public function get_list_pins_files()
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
        
        $shell = new Shell();
        $retval = $shell->execute(self::CURL_COMMAND, ' -X GET "'.self::API_BASE_URL.'pins" -H "accept: application/json"', TRUE, $options);
        $lines = $shell->get_output();

        return $this->_filter_response($lines, 3);

    }

    /**
     * Pin the specified IPFS address to the local node
     *
     * @param string $cid cid for file
     *
     * @return array pins information
     */

    public function put_pins_file($cid)
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::CURL_COMMAND, ' -X PUT "'.self::API_BASE_URL.'pins/'.$cid.'" -H "accept: application/json"', TRUE, $options);
        $lines = $shell->get_output();

        $error = (preg_match('/An unhandled exception occurred/', $lines[3])) ? 'An unhandled exception occurred!' : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);

        }
    }

    /**
     * Unpins the IPFS address from the local node
     *
     * @param string $cid cid for file
     *
     * @return array unpins information
     */

    public function unpins_file($cid)
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::CURL_COMMAND, ' -X DELETE "'.self::API_BASE_URL.'pins/'.$cid.'" -H "accept: application/json"', TRUE, $options);
        $lines = $shell->get_output();

        $error = (preg_match('/An unhandled exception occurred/', $lines[3])) ? 'An unhandled exception occurred!' : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);

        }
    }

    /**
     * Retrieve a list of the currently configured base directories of the *live* server instance
     *
     * @return array Retrieve a list of the currently configured base directories 
     */

    public function get_basedirs()
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::CURL_COMMAND, ' -X GET "'.self::API_BASE_URL.'basedirs" -H "accept: application/json"', TRUE, $options);
        $lines = $shell->get_output();

        $error = (preg_match('/An unhandled exception occurred/', $lines[3])) ? 'An unhandled exception occurred!' : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);

        }
    }

    /**
     * Add an additional base directory to the list of configured ones
     *
     * @param string $key  key for a path
     * @param string $path path for additional base directory
     *
     * @return array Retrieve a list of the currently configured base directories 
     */

    public function add_base_directory($key, $path)
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::CURL_COMMAND, '-X POST "'.self::API_BASE_URL.'basedirs" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"key\": \"'.$key.'\", \"path\": \"'.$path.'\"}"', TRUE, $options);
        $lines = $shell->get_output();
        $error = (preg_match('/An unhandled exception occurred/', $lines[3])) ? 'An unhandled exception occurred!' : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);

        }
    }

    /**
     * Remove the base directory configured under `key` from the *live* server instance
     *
     * @param string $key key for file
     *
     * @return array directory lists
     */

    public function delete_directory($key)
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        $shell = new Shell();
        $retval = $shell->execute(self::CURL_COMMAND, ' -X DELETE "'.self::API_BASE_URL.'basedirs/'.$key.'" -H "accept: application/json"', TRUE, $options);
        $lines = $shell->get_output();

        $error = (preg_match('/An unhandled exception occurred/', $lines[3])) ? 'An unhandled exception occurred!' : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);

        }
    }

    /**
     * Get the contents of directory in the cluster storage.
     *
     * @param  string $key directory key name
     *
     * @return array cluster storage.
     */

    public function get_list_contents($key)
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------
        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';
            
        $shell = new Shell();
        $retval = $shell->execute(self::CURL_COMMAND, ' -X POST "'.self::API_BASE_URL.'browse/'.$key.'" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"page\": 0, \"pages\": 0, \"per_page\": 0, \"total\": 0}"', TRUE, $options);

        $lines = $shell->get_output();

        $error = (preg_match('/An unhandled exception occurred/', $lines[3])) ? 'An unhandled exception occurred!' : NULL;
        
        if ($error) {
            throw new Engine_Exception($error);
        
        } else {
            return $this->_filter_response($lines, 3);

        }
    }

    /**
     * Add the local file included in the PUT request path to the IPFS cluster
     *
     * @param string $name              file name
     * @param string $request_file_path file path
     *
     * @return array local cluster storage.
     */

    public function put_file_to_ipfs_cluster($name, $request_file_path)
    {

        clearos_profile(__METHOD__, __LINE__);

        //----------------------------------------------

        $options['validate_exit_code'] = FALSE;
        $options['env'] = 'LANG=en_US';

        //-------------------------------
        //PUT request path to the IPFS cluster

        $shell = new Shell();

        $retval = $shell->execute(self::CURL_COMMAND, ' -X PUT "'.self::API_BASE_URL.'files/'.$request_file_path.'?name='.$name.'" -H "accept: application/json"', TRUE, $options);
        $lines = $shell->get_output();

        $error = (preg_match('/An unhandled exception occurred/', $lines[3])) ? 'An unhandled exception occurred!' : NULL;

        if ($error) {
            throw new Engine_Exception($error);
        
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    // P R O T E C T E D   M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Filter response
     *
     * @param string $response output response
     * @param string $index    index of array
     *
     * @return array response data
     */

    protected function _filter_response($response, $index =3)
    {
        $data = json_decode($response[$index], TRUE);
        return $data;
    }
}
